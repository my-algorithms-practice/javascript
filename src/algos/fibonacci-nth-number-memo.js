export const fibonacci = (n, memo = [0, 1]) => {
  let output = memo[n];

  if (typeof output !== 'number') {
    output = fibonacci(n - 1, memo) + fibonacci(n - 2, memo);
    memo[n] = output;
  }

  return output;
}

export const fibonacciNonES6 = (n) => {
  let memo = [0, 1];

  let output = memo[n];

  if (typeof output !== 'number') {
    output = fibonacci(n - 1) + fibonacci(n - 2);
    memo[n] = output;
  }

  return output

}
