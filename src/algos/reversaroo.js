let alphabet = ["A", 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', "L", "M", "N",
  "O", "P", "Q", 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

export const reversaroo = () => {
  console.log('before:' + alphabet)
  for (let i = 0; i < alphabet.length / 2; i++) {
    alphabet[i] = alphabet[alphabet.length - i - 1]
  }
  console.log('after:' + alphabet)
}


// reversaroo();