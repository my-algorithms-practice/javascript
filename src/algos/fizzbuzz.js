let alphabet = ["A", 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', "L", "M", "N",
  "O", "P", "Q", 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

export const fizzbuzz = () => {
  alphabet.forEach((letter, counter) => {
    let printer = letter;
    if ((counter + 1) % 3 === 0) {
      printer = 'fizz'
    } else if ((counter + 1) % 5 === 0) {
      printer = 'buzz'
    }
    if (printer === 'fizz' && ((counter + 1) % 5 === 0)) {
      printer += 'buzz';
    }

    console.log(printer)

  })

}


