import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import { useEffect, useReducer } from 'react';
import SelectedAlgoContext, { SelectedAlgoContextReducer } from './state/SelectedAlgo';


function App() {

  const [selectedAlgorithmId, setSelectedAlgorithmId] = useReducer(SelectedAlgoContextReducer);

  useEffect(() => {
    console.log('new id value', selectedAlgorithmId)
  }, [selectedAlgorithmId])


  return (
    <div className="App">
      <header className="App-header">
        <Header />
      </header>
      <SelectedAlgoContext.Provider value={{ selectedAlgorithmId, setSelectedAlgorithmId }}>
        <Body />
      </SelectedAlgoContext.Provider>
      <Footer />
    </div>
  );
}

export default App;
