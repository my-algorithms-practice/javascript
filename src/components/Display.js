import { fibonacci, fibonacciNonES6 } from "../algos/fibonacci-nth-number-memo";

export default function Selection() {

  return (
    <div className="js-algo-cardstock container">
      <h3>Result:</h3>
      <p>{fibonacci(10)}</p>
      <p>{fibonacciNonES6(10)}</p>
    </div>
  )
}