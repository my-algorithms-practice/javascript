import algoslist from '../config/listofalgos.json';
import SelectedAlgoContext from '../state/SelectedAlgo';
import { useContext, useEffect } from 'react';

export default function Selection() {
  const { selectedAlgorithmId, setSelectedAlgorithmId } = useContext(SelectedAlgoContext);
  console.log('start', selectedAlgorithmId);

  useEffect(() => {
    console.log('new id value', selectedAlgorithmId)
  }, [selectedAlgorithmId])


  let mappedList = algoslist.funcs.map((item, key) => {
    return (
      <option value={item.id} key={key}>{item.name}</option>
    )
  })

  return (
    <div className="js-algo-cardstock container">
      <h3>Please choose a solution to review:</h3>
      <select
        className="form-select"
        aria-label="Algorithm selection"
        onChange={(e) => {
          setSelectedAlgorithmId(e.target.value)
          console.log('change', e.target.value);
        }
        }
        value={selectedAlgorithmId}
      >
        {mappedList}
      </select>
      <br />
      {selectedAlgorithmId === 1 && <div> level 1</div>}
      {selectedAlgorithmId === 2 && <div> level 2</div>}
      {selectedAlgorithmId === 3 && <div> level 3</div>}

    </div>
  )
}