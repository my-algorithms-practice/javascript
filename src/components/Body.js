import Selection from './Selection';
import Display from './Display';

export default function Body() {


  return (
    <div className="js-algo-body">
      <div className="container px-4 text-center">
        <div className="row gx-5">
          <div className="col">
            <div className="p-3">
              <Selection />
            </div>
          </div>
          <div className="col">
            <div className="p-3">
              <Display />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}