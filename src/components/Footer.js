export default function Footer() {

  let footerStyles =
  {
    textDecoration: 'none',
    color: 'black',
  }

  return (
    <div className='footer'>
      &copy; 2023 <a href="https://simulatebaseball.com" target='tab' style={footerStyles}>Zachary Koch</a>
    </div>
  )
}